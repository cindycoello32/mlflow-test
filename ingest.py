import warnings
import pandas as pd
import numpy as np
import mlflow
import logging
from matplotlib import pyplot

def ingest():

    # Read the wine-quality csv file from the URL
    csv_url = (
        "http://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/winequality-red.csv"
    )
    try:
        data = pd.read_csv(csv_url, sep=";")
        return data
    except Exception as e:
        print(
            "Unable to download training & test CSV, check your internet connection. Error: %s", e
        )
