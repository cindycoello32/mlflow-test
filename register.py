from urllib.parse import urlparse
import mlflow
import mlflow.sklearn    
import yaml
from yaml.loader import SafeLoader

from train.predict import (predict)

def register():

    pre = predict()
    run = pre[0]
    lr = pre[1]
    rmse = pre[2]
    mae = pre[3]
    r2 = pre[4]
    alpha = pre[5]
    l1_ratio = pre[6]

    print("IM THE SAME RUN: ", run)
    with mlflow.start_run(run_id=run):

        print("Elasticnet model (alpha=%f, l1_ratio=%f):" % (alpha, l1_ratio))
        print("  RMSE: %s" % rmse)
        print("  MAE: %s" % mae)
        print("  R2: %s" % r2)

        mlflow.log_param("alpha", alpha)
        mlflow.log_param("l1_ratio", l1_ratio)
        mlflow.log_metric("rmse", rmse)
        mlflow.log_metric("r2", r2)
        mlflow.log_metric("mae", mae)

        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme

        # Model registry does not work with file store
        if tracking_url_type_store != "file":
            mlflow.sklearn.log_model(lr, "model", registered_model_name="ElasticnetWineModel")
        else:
            mlflow.sklearn.log_model(lr, "model")