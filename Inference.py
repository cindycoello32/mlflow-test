import mlflow
import mlflow.sklearn

from train.train import (train)
from train.evaluate import (eval_metrics)


def predict():
    training = train()
    train_x = training[1]
    train_y = training[3]
    alpha = training[4]
    l1_ratio = training[5]

    with mlflow.start_run() as run:
        run_id = str(run.info.run_id)
        print ("IM THE RUN ID: ", run_id)

        model_name = "ElasticnetWineModel"
        stage = 'Production'

        sk_model = mlflow.pyfunc.load_model(model_uri=f"models:/{model_name}/{stage}")

        predicted_qualities = sk_model.predict(train_x)

        (rmse, mae, r2) = eval_metrics(train_y, predicted_qualities)

        mlflow.end_run()

    return run_id, rmse, mae, r2, alpha, l1_ratio 