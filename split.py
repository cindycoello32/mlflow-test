from this import d
from sklearn.model_selection import train_test_split
import mlflow
import mlflow.sklearn

from train.ingest import (ingest)


def split():

    data = ingest()
    # Split the data into training and test sets. (0.75, 0.25) split.
    train, test = train_test_split(data)

    return train, test